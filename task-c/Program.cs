﻿using System;

namespace task_c
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintGreeting("Sam");
            PrintGreeting("James");
            PrintGreeting("Andrew");
            PrintGreeting("Damian");
            PrintGreeting("Jeff");
        }
        public static string PrintGreeting( string name )
        {
            return $"Hello {name}";
        }
    }
}
